export function loremIpsumGenerator(max = 7) {
  const words =
    `Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris neque urna, pellentesque a urna non, convallis interdum lectus. Pellentesque feugiat tortor ex, in consequat metus feugiat fermentum. Etiam odio felis, varius ac justo vel, condimentum luctus erat. Phasellus et purus a quam maximus euismod. In eu augue ut metus auctor porta id in enim. Praesent mattis semper sagittis. Nunc non leo ac mauris condimentum tristique at in sem. Quisque vehicula non enim a bibendum. Nunc sed nunc lobortis, sodales enim sed, scelerisque massa. Integer semper est a urna vulputate, a consequat enim iaculis. Etiam convallis, dui ac rhoncus maximus, lacus ex gravida est, in convallis sapien neque eget mi. Integer eget molestie sem, id sollicitudin orci.
    Nullam eget sem fermentum, iaculis sapien non, dapibus tortor. Phasellus vel vestibulum tortor, id varius leo. Ut ut ipsum id erat gravida pharetra eget vitae dui. Suspendisse tellus massa, aliquet in mi id, fringilla elementum metus. Vivamus porttitor ut sem vitae placerat. Aliquam vitae ligula quis nisl laoreet cursus suscipit a nunc. Nullam porttitor tristique tellus, at bibendum nisl convallis nec. Sed porttitor, tellus eu dictum pharetra, ipsum nulla tincidunt orci, a rhoncus nisl enim id arcu.
    Aenean nulla massa, aliquet sodales rutrum eget, fringilla non metus. Donec ullamcorper velit eget dui semper, sit amet tincidunt dolor elementum. Ut a consectetur mi. Phasellus sagittis, ex at venenatis varius, lacus dui suscipit sem, quis vulputate eros nisl nec ex. Vivamus sodales diam nec lacus sodales, ut rhoncus dui fermentum. Mauris eget dui mi. Aliquam tincidunt erat eleifend, facilisis mi id, dapibus nulla. Integer dignissim gravida accumsan. Donec vestibulum arcu eget congue ornare.
    Sed posuere in dui in ultricies. Fusce vehicula aliquam orci ac elementum. Nunc egestas varius mi, sit amet luctus ex dictum vitae. Morbi efficitur pellentesque massa facilisis varius. Sed laoreet eget tortor et tincidunt. Maecenas lobortis, ligula nec pulvinar interdum, ipsum risus cursus quam, nec tincidunt magna dolor quis augue. Duis scelerisque euismod ipsum nec ultricies. Quisque euismod semper quam et egestas. Aenean eleifend sed nisl ac maximus. Cras arcu tortor, pellentesque id nulla quis, ornare dignissim turpis. Pellentesque id nulla at ante mattis aliquam. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Aenean et massa accumsan, viverra neque in, congue libero. Fusce vel dui at massa fermentum commodo. Cras dignissim metus massa, eu iaculis lectus fermentum venenatis. Aliquam facilisis ut eros sit amet maximus.
    Nunc leo purus, pellentesque at velit id, commodo facilisis risus. Ut venenatis orci a enim ullamcorper, non malesuada leo posuere. Curabitur rutrum, felis non imperdiet fringilla, nunc ligula dapibus massa, vel egestas erat massa eu felis. Maecenas dignissim a ex et imperdiet. Morbi vestibulum lacinia odio, et imperdiet est laoreet ac. Curabitur maximus pulvinar lectus quis molestie. Cras sagittis sapien sed convallis pulvinar. Proin eu dolor dictum magna consequat porttitor sit amet vel libero.
    Interdum et malesuada fames ac ante ipsum primis in faucibus. Pellentesque sollicitudin tempor turpis ut elementum. Sed at erat vel enim rutrum condimentum sed ut turpis. Mauris eu est.`
      .replace(/\,|\./g, "")
      .split(" ");

  return words
    .splice(
      Math.ceil(Math.random() * words.length - 1),
      Math.ceil(Math.random() * max + 1)
    )
    .join(" ");
}

export function cardDataGenerator() {
  return {
    name: loremIpsumGenerator(),
    minPrice: null,
    maxPrice: null,
    locationTypeIds: [],
    locationCuisineTypeIds: [],
    dishTypeIds: [],
    courseTypeIds: [],
    dietIds: [],
    excludedIngredientIds: [],
  };
}
