export const globalStyles = {
  shadow: {
    shadowColor: "#b0b0b0b",
    shadowOffset: {
      width: 0,
      height: 1,
    },
    shadowOpacity: 0.1,
    shadowRadius: 7,
    elevation: 5,
  },
};
