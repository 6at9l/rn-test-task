export interface ActiveCardInterface {
  name: string;
  onClose: () => void;
  onShare: () => void;
  onDuplicate: () => void;
  onDelete: () => void;
}
