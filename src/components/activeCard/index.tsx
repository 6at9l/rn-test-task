import { View, Image, Text, TouchableOpacity } from "react-native";
import { BlurView } from "expo-blur";
import { FoodCard } from "../card/index";
import { styles } from "./styles";
import { ActiveCardInterface } from "./interface";
import { ICON_TYPE } from "../card/interface";

export const ActiveCard: React.FC<ActiveCardInterface> = ({
  name,
  onClose,
  onShare,
  onDuplicate,
  onDelete,
}) => {
  return (
    <View style={styles.mainContainer}>
      <BlurView style={styles.blurContainer} intensity={50}>
        <Image
          style={styles.blurImage}
          source={require("../../../assets/background/background.png")}
          blurRadius={100}
        />
      </BlurView>
      <View style={styles.contentContainer}>
        <FoodCard name={name} onPress={onClose} iconType={ICON_TYPE.CLOSE} />
      </View>

      <View style={styles.actionContainer}>
        <View style={styles.actionRow}>
          <Text style={styles.actionText}>Share</Text>
          <TouchableOpacity onPress={onShare}>
            <Image
              source={require("../../../assets/icons/share.png")}
              resizeMethod="resize"
            />
          </TouchableOpacity>
        </View>
        <View style={styles.actionRow}>
          <Text style={styles.actionText}>Duplicate</Text>
          <TouchableOpacity onPress={onDuplicate}>
            <Image
              source={require("../../../assets/icons/duplicate.png")}
              resizeMethod="resize"
            />
          </TouchableOpacity>
        </View>
        <View style={styles.actionRow}>
          <Text style={styles.actionText}>Delete</Text>
          <TouchableOpacity onPress={onDelete}>
            <Image
              source={require("../../../assets/icons/delete.png")}
              resizeMethod="resize"
            />
          </TouchableOpacity>
        </View>
      </View>
    </View>
  );
};
