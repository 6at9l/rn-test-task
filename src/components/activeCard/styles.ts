import { StyleSheet, Dimensions } from "react-native";
const { width, height } = Dimensions.get("window");

export const styles = StyleSheet.create({
  mainContainer: {
    flex: 1,
  },
  blurContainer: {
    position: "absolute",
    width,
    height,
  },
  blurImage: { height: "100%", width: "100%" },

  contentContainer: {
    flex: 2,
    flexDirection: "column-reverse",
    paddingBottom: 10,
  },
  actionContainer: { flex: 3, paddingHorizontal: 20 },
  actionRow: {
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "flex-end",
  },
  actionText: {
    color: "rgb(17, 183, 119)",
    fontSize: 15,
  },
});
