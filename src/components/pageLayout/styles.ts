import { StyleSheet, Dimensions } from "react-native";
import { globalStyles } from "../styles";

const { width, height } = Dimensions.get("screen");

export const styles = StyleSheet.create({
  mainContainer: {
    flex: 1,
    backgroundColor: "rgb(248, 248, 248)",
  },
  headerContainer: {
    position: "absolute",
    height: height * 0.25,
    width,
  },
  contentContainer: {
    flex: 1,
    marginTop: height * 0.02,
  },
  actionContainer: {
    minHeight: 68,
    flexDirection: "column-reverse",
    alignItems: "center",
    backgroundColor: "transparent",
  },
  action: {
    top: 0,
    zIndex: 1,
    position: "absolute",
    width: width - 40,
  },
  bottomDecoration: {
    backgroundColor: "white",
    height: 50,
    width,
    ...globalStyles.shadow,
  },
  logo: {
    width: 20,
    height: 25,
    marginLeft: 20,
    marginTop: 10,
  },
});
