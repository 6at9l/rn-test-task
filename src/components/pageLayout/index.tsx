import { View, SafeAreaView, Image } from "react-native";
import { LinearGradient } from "expo-linear-gradient";
import { styles } from "./styles";
import { HeaderLayoutInterface } from "./interface";

export const HeaderLayout: React.FC<HeaderLayoutInterface> = ({
  children,
  actionComponent,
}) => {
  return (
    <View style={styles.mainContainer}>
      <LinearGradient
        start={{ x: 0, y: 0.75 }}
        end={{ x: 1, y: 0.25 }}
        colors={["rgba(250, 119, 69, 1)", "rgba(243, 196, 66, 1)"]}
        style={styles.headerContainer}
      />
      <SafeAreaView>
        <Image
          style={styles.logo}
          source={require("../../../assets/icons/logo.png")}
        />
      </SafeAreaView>
      <View style={styles.contentContainer}>{children}</View>
      <View style={styles.actionContainer}>
        <SafeAreaView style={styles.action}>{actionComponent}</SafeAreaView>
        <View style={styles.bottomDecoration} />
      </View>
    </View>
  );
};

{
  /* <LinearGradient
      start={{ x: 0, y: 0.5 }}
      end={{ x: 1, y: 0.5 }}
      colors={[
        "rgba(250, 119, 69, 1)",
        "rgba(243, 196, 66, 1)",
        "rgba(248, 248, 248, 1)",
        "rgba(248, 248, 248, 1)",
      ]}
      locations={[0, 0.2, 0.2, 1]}
      style={styles.mainContainer}
    >
     
        <View></View>
        <View>{children}</View>
       
      </SafeAreaView>
    </LinearGradient> */
}
