export interface HeaderLayoutInterface {
  children?: React.ReactNode;
  actionComponent?: React.ReactNode;
}
