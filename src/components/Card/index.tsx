import { View, Text, Image, TouchableOpacity } from "react-native";
import { FoodCardProps, AddCardProps, ICON_TYPE } from "./interface";
import { styles } from "./styles";

import { useMutation } from "@apollo/client";
import { cardDataGenerator } from "../../helpers";
import { useMemo } from "react";

export const FoodCard: React.FC<FoodCardProps> = ({
  name,
  onPress,
  iconType = ICON_TYPE.OPTION,
}) => {
  const img = useMemo(
    () => (
      <Image
        source={
          iconType === ICON_TYPE.OPTION
            ? require("../../../assets/icons/options.png")
            : require("../../../assets/icons/close.png")
        }
        style={styles.actionIcon}
      />
    ),
    [iconType]
  );

  return (
    <View style={[styles.cardContainer, styles.foodCard]}>
      <View style={styles.textContainer}>
        <Text style={styles.cardText} numberOfLines={2}>
          {name}
        </Text>
      </View>
      <View>
        <TouchableOpacity onPress={onPress}>{img}</TouchableOpacity>
      </View>
    </View>
  );
};

export const AddFoodCard: React.FC<AddCardProps> = ({
  name = "New Food Style",
  onPress,
  disabled = false,
}) => {
  return (
    <TouchableOpacity onPress={onPress} disabled={disabled}>
      <View
        style={[
          { padding: 15, maxHeight: 50, transform: [{ translateY: -10 }] },
          styles.cardContainer,
        ]}
      >
        <Image
          source={require("../../../assets/icons/add.png")}
          resizeMethod="resize"
          style={[styles.addIcon]}
        />
        <Text style={styles.cardText}>{name}</Text>
      </View>
    </TouchableOpacity>
  );
};
