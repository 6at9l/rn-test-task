export interface FoodCardProps {
  name: string;
  onPress: () => void;
  iconType?: ICON_TYPE;
}

export interface AddCardProps {
  name?: string;
  onPress: () => void;
  disabled?: boolean;
}

export enum ICON_TYPE {
  OPTION = "options",
  CLOSE = "close",
}
