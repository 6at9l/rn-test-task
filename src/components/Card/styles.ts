import { StyleSheet } from "react-native";
import { globalStyles } from "../styles";

export const styles = StyleSheet.create({
  foodCard: {
    marginVertical: 6,
    marginHorizontal: 20,
    paddingVertical: 5,
    paddingHorizontal: 20,
    justifyContent: "space-between",
  },
  textContainer: {
    flex: 1,
    paddingRight: 28,
  },
  cardContainer: {
    minHeight: 48,
    backgroundColor: "white",
    borderRadius: 6,
    flexDirection: "row",
    alignItems: "center",
    ...globalStyles.shadow,
  },
  cardText: {
    fontSize: 18,
    fontWeight: "500",
    color: "rgb(67, 67, 67)",
    lineHeight: 22,
  },
  addIcon: { width: 32, height: 32 },
  actionIcon: { width: 24, height: 24 },
});
