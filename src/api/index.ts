import { useMutation } from "@apollo/client";
import { useEffect } from "react";
import { Share } from "react-native";

import { cardDataGenerator } from "../helpers";
import { ApiInterfaceParams, ApiInterfaceReturn } from "./interface";
import {
  ADD_CARD,
  DELETE_CARD,
  DUPLICATE_CARD,
  GET_CARDS,
  SHARE_CARD,
} from "./query";

export * from "./query";

export const useCRUD = ({
  afterShare,
  afterAdd,
}: ApiInterfaceParams): ApiInterfaceReturn => {
  const [addMutation, { loading: addLoading }] = useMutation(ADD_CARD, {
    refetchQueries: [{ query: GET_CARDS }],
    onCompleted(data) {
      afterAdd();
    },
  });
  const [shareMutation, { loading: shareLoading, data: shareData, reset }] =
    useMutation(SHARE_CARD);

  const [duplicateMutation, { loading: duplicateLoading }] = useMutation(
    DUPLICATE_CARD,
    {
      refetchQueries: [{ query: GET_CARDS }],
    }
  );
  const [deleteMutation, { loading: deleteLoading }] = useMutation(
    DELETE_CARD,
    {
      refetchQueries: [{ query: GET_CARDS }],
    }
  );

  useEffect(() => {
    (async function () {
      if (shareData?.shareCard) {
        const res = await Share.share({
          message: `https://cards.foodstyles.com/${shareData?.shareCard}`,
        });

        if (res.action !== "dismissedAction") afterShare();
        reset();
      }
    })();
  }, [shareData]);

  return {
    loading: addLoading || shareLoading || duplicateLoading || deleteLoading,
    addMutation: () =>
      addMutation({ variables: { cardData: cardDataGenerator() } }),

    shareMutation: (id) => shareMutation({ variables: { id } }),
    duplicateMutation: (id) => duplicateMutation({ variables: { id } }),
    deleteMutation: (id) => deleteMutation({ variables: { id } }),
  };
};
