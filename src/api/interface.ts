export interface ApiInterfaceReturn {
  loading: boolean;
  addMutation: () => void;
  shareMutation: (id: number | string) => void;
  duplicateMutation: (id: number | string) => void;
  deleteMutation: (id: number | string) => void;
}

export interface ApiInterfaceParams {
  afterShare: () => void;
  afterAdd: () => void;
}
