export interface Card {
  name: string;
  id: number | string;
}
