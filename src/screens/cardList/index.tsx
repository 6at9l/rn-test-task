import React, { useCallback, useState, useRef } from "react";
import { Alert, FlatList, Modal } from "react-native";
import { FoodCard, AddFoodCard } from "../../components/card";
import { HeaderLayout } from "../../components/pageLayout";
import { useQuery } from "@apollo/client";

import { ActiveCard } from "../../components/activeCard";
import { Card } from "./interface";
import { GET_CARDS, useCRUD } from "../../api";

export const CardList = () => {
  const ref = useRef<FlatList>();
  const { loading: loadingData, data } = useQuery(GET_CARDS, {});
  const [selectedCard, setSelectedCard] = useState<Card>(null);

  const {
    loading,
    shareMutation,
    duplicateMutation,
    deleteMutation,
    addMutation,
  } = useCRUD({
    afterShare: () => {
      setSelectedCard(null);
    },
    afterAdd: () => {
      ref.current.scrollToEnd({ animated: false });
    },
  });

  const onAdd = useCallback(() => {
    addMutation();
  }, []);

  const onClose = useCallback(() => {
    setSelectedCard(null);
  }, []);

  const onShare = useCallback(async () => {
    shareMutation(selectedCard?.id);
  }, [selectedCard]);

  const onDuplicate = useCallback(() => {
    duplicateMutation(selectedCard?.id);
    onClose();
  }, [selectedCard]);

  const onDelete = useCallback(() => {
    Alert.alert(
      "Confirm delete",
      "This will delete the Food Style and all its settings.",
      [
        {
          text: "Delete",
          onPress: () => {
            deleteMutation(selectedCard?.id);
            onClose();
          },
        },
        {
          text: "Cancel",
          onPress: () => {},
        },
      ]
    );
  }, [selectedCard]);

  return (
    <HeaderLayout actionComponent={<AddFoodCard onPress={onAdd} />}>
      <Modal visible={!!selectedCard} transparent={true}>
        <ActiveCard
          name={selectedCard?.name}
          onClose={onClose}
          onShare={onShare}
          onDuplicate={onDuplicate}
          onDelete={onDelete}
        />
      </Modal>
      <FlatList
        ref={ref}
        data={data?.cards || []}
        keyExtractor={({ id }) => id}
        renderItem={({ item: { name, id } }) => (
          <FoodCard name={name} onPress={() => setSelectedCard({ id, name })} />
        )}
      />
    </HeaderLayout>
  );
};
