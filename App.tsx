import { NavigationContainer } from "@react-navigation/native";
import { createNativeStackNavigator } from "@react-navigation/native-stack";
import {
  ApolloClient,
  InMemoryCache,
  ApolloProvider,
  createHttpLink,
} from "@apollo/client";
import { setContext } from "@apollo/client/link/context";

import { CardList } from "./src/screens/cardList";

const httpLink = createHttpLink({
  uri: "https://api-dev.foodstyles.com/graphql",
});

const authLink = setContext((_, { headers }) => {
  const token =
    "eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6NDk3LCJ0eXBlIjoiQUNDRVNTIiwiaWF0IjoxNjU1MTE0ODA4LCJleHAiOjE2NTU3MTk2MDh9.LS6GcOi6zNLIJn5B7a_SmRmUvQSF7azPu-Utr7LUAGwAYiL175fxkOHp-DwYstWNzX7udEI9-SDWd9--kxVrKf4_YOjJPp7xmrnpI6iRxU8599Gq4NESDL0GQ8owpA-zeOFlkdCzqTdPS0I74pGpfJ83SL99VgBI1HN5UIzq4VYWyDyVaz2yEVkmU5dwyxVDoQ9ByKez7bx3_t3qLMCE36L6jx9kbH_gMQm9koQA6hFmIlPOevqbMVUttzv6qhRVBvksTLM_tveQQQ49FFu5598pQny1MBL9sVOxvppTISs6YxVFoSpQoY196f1zDYGdHTP9mV2wQi5ouHMKSiq6YQ";
  return {
    headers: {
      ...headers,
      authorization: token ? `Bearer ${token}` : "",
    },
  };
});

const client = new ApolloClient({
  cache: new InMemoryCache(),
  link: authLink.concat(httpLink),
});

const Stack = createNativeStackNavigator();

export default function App() {
  return (
    <ApolloProvider client={client}>
      <NavigationContainer>
        <Stack.Navigator
          screenOptions={{
            headerShown: false,
          }}
        >
          <Stack.Screen name="CardList" component={CardList} />
        </Stack.Navigator>
      </NavigationContainer>
    </ApolloProvider>
  );
}
